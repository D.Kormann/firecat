﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Delegates : MonoBehaviour
{
    public delegate void del();
    // Start is called before the first frame update
    void Start()
    {
        del F ;
        F = new del (f2)+f1;


        del[] A=new del[3];
        A[0] = new del(f1);
        A[0]();
        Func<int, int, bool> isSq = (x,y) => x ==y*y;
        A[1] = new del(() => print(33));

        Action ac;
        ac = () => print("nothing");

        Action<int>[] acA=new Action<int>[4];
        acA[1](2);

        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void f1()
    {
        print(1);
    }
    void f2()
    {
        print(2);
    }
    
    
}
