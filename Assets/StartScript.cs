﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartScript : MonoBehaviour
{
    public void StartGame()
    {
        Core.Fury = 0;
        Core.PLevel = 1;
        Core.CLevel = 1;
        Core.nice = true;
        SaveSystem.safe();
        SceneManager.LoadScene("1");
    }
}
