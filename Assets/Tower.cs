﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Tower : MonoBehaviour
{
    [SerializeField] Transform middleground;
    [SerializeField] Transform baseground;
    float ceilingHeight;
    Transform Cam;
    int count;
    static Transform theTower;
    private void Start()
    {
        theTower = transform;
        ceilingHeight = baseground.position.y- middleground.position.y;
        Cam = Camera.main.transform;
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.tag == "Player")
        {
            SceneManager.UnloadSceneAsync(3);
            Figure.player.transform.Translate(Vector3.up * ceilingHeight-transform.position);
            Cam.Translate(Vector3.up * ceilingHeight-transform.position);
            count++;
            SceneManager.LoadSceneAsync(3, LoadSceneMode.Additive);
            transform.position = Vector3.zero;
        }
    }
    public static void nextSide(Vector3 nextPos)
    {
        theTower.position = nextPos;
    }
}
