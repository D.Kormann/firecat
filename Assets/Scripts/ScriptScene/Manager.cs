﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour
{

    public static Manager manager;
    public Container[] containers;
    int CC;
    public Container[] plan = new Container[3];
    public int pointer;
    public static int Stage;
    //UI Management
    public GameObject panel;
    public Text Food, itsLevel, myLevel, itsFood, myFood;

    private void Awake()
    {
        manager = this;
    }
    private void Start()
    {
        CC = containers.Length;
        pointer = 1;
        
        int R = Random.Range(0, CC);
        plan[1] = Instantiate(containers[R], plan[0].transform.position + Vector3.right * 50, Quaternion.identity);
        plan[1].index = 1;
        R = Random.Range(0, CC);
        plan[2] = Instantiate(containers[R], plan[1].transform.position + Vector3.right * 50, Quaternion.identity);
        plan[2].index = 2;



    }

    //MapErweiterung
    public void OnPass(int container)
    {
        int offset = (container - pointer + 3) % 3;
        if (offset == 1)
        {
            pointer = (pointer + 1) % 3;
            int temp = (pointer - 2 + 3) % 3;
            Destroy(plan[temp].gameObject);
            int R = Random.Range(0, CC);

            plan[temp] = Instantiate(containers[R], plan[pointer].transform.position + Vector3.right * 50, Quaternion.identity);
            plan[temp].index = temp;
        }else if (offset == 2)
        {
            pointer = (pointer -1+3) % 3;
            int temp = (pointer + 2 + 3) % 3;
            Destroy(plan[temp].gameObject);
            int R = Random.Range(0, CC);

            plan[temp] = Instantiate(containers[R], plan[pointer].transform.position + Vector3.left * 50, Quaternion.identity);
            plan[temp].index = temp;
        }
        
    }

    //CompanyPanel
    public void Pause()
    {
        panel.SetActive(true);
        Time.timeScale = 0;
        Food.text = Interface.FoodCount + " Food.";
        itsLevel.text = "Cat Level " + Figure.pet.Level;
        myLevel.text = "Player Level " + Figure.player.Level;

    }
    public void Resume()
    {
        panel.SetActive(false);
        Time.timeScale = 1;
    }

    public void Feed()
    {
        if (Interface.FoodCount > 0)
        {
            Interface.FoodCount--;
            Food.text = Interface.FoodCount + " Food.";
            Figure.pet.XP++;
            Figure.pet.Fury -= 100;
            if (Figure.pet.Fury < 0)
            {
                Figure.pet.Fury = 0;
            }
            
        }
    }
    //PlayerPanel
    public void Eat()
    {
        if (Interface.FoodCount > 0)
        {
            Interface.FoodCount--;
            Food.text = Interface.FoodCount + " Food.";
            Figure.player.XP++;
            if (Figure.player.XP > Figure.player.MaxXP)
            {
                Figure.player.Level++;
                Figure.player.XP = 0;
                myLevel.text = "Player Level " + Figure.player.Level;

            }
        }
    }
}
