﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Interface : MonoBehaviour
{
    //Map Management
    public Container[] Containers;

    //Recource management
    private Player player;
    private Company pet;
    public Transform HPBar;
    public Transform ManaBar;
    public Transform FuryBar;
    public Text Health;
    public Text Manapool;
    public Text Level;
    public Text Food;
    float HPP, ManaP;
    public static int FoodCount;
    public void Start()
    {
        ManaP = HPP = 1;
        player = Figure.player;
        pet = Figure.pet;
        player.Mana = player.MaxMana;
        player.HP = player.MaxHP; 
    }
    public void Update()
    {
        HPP = player.HP / player.MaxHP;
        ManaP = player.Mana / player.MaxMana;
        
        HPBar.localScale = new Vector3(HPP, 1, 1);
        HPBar.transform.localPosition = Vector3.right *( HPP / 2-.5f);
        Health.text = "HP " + (int)player.HP + "/" + player.MaxHP;
        ManaBar.localScale = new Vector3(ManaP, 1, 1);
        ManaBar.transform.localPosition= Vector3.right * (  ManaP / 2-.5f);
        Manapool.text = "Mana" + (int)player.Mana + "/" + player.MaxMana;
        FuryBar.localScale = Vector3.one * (pet.Fury / pet.MaxFury);
        Level.text = "player LVL " + player.Level + "\npet LVL " + Figure.pet.Level;
        Food.text = "Food:\n" + FoodCount;
    }
    
}
