﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenContainer : Container
{

    public Opponent block;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 1; i < 5; i++)
        {
            Opponent b;
            float f = Random.Range(0f, 1f);
            Vector3 pos = startPoint * f + endPoint*(1 - f);
            b = Instantiate(block, pos, Quaternion.identity);
            b.transform.SetParent(transform);
            b.Spawn(i);
        }
    }
}
