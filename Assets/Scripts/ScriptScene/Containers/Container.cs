﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Container : MonoBehaviour
{
    public int index;
    public Vector3 startPoint, centerPoint, endPoint;


    private void Awake()
    {
        
        startPoint = transform.Find("StartPoint").position;
        centerPoint = transform.Find("CenterPoint").position;
        endPoint = transform.Find("EndPoint").position;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {


            Manager.manager.OnPass(index);
        }
    }
}
