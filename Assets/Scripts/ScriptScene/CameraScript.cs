﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour
{
    [SerializeField] Transform sky;
    public Transform Player;
    private Vector3 offset;
    private Vector3 skyOffset;
    // Start is called before the first frame update
    void Start()
    {
        Player = FindObjectOfType<Player>().transform;
        offset = transform.position - Player.position;
        skyOffset=sky.position-transform.position;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        sky.position = (Player.position / 2) + skyOffset ;
        /*
        if (skyOffset.x - Player.position.x > 1)
        {
            skyOffset += Vector3.left;
        }
        else if (skyOffset.x - Player.position.x < 1)
        {
            skyOffset += Vector3.right;
        }
        */
        transform.position = Vector3.Lerp(Player.position + offset, transform.position, 0.1f);

    }
}
