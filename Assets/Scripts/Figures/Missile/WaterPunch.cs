﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaterPunch : Missile
{
    private float frozenSpeed;
    public Opponent f;
    
    protected override void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == targetTag)
        {
            Destroy(transform.GetChild(0).gameObject);
            Destroy(GetComponent<Collider2D>());
            transform.localScale *= 3;
            speed = 0;
            transform.SetParent(collision.transform);
            f = transform.parent.GetComponent<Opponent>();
            frozenSpeed = f.speed;
            f.nice = true;
            startTime = Time.time;
            duration = 2;
        }
    }
    private void OnDestroy()
    {
        if (f !=null)
        {
            f.speed = frozenSpeed;
            f.nice = false;
        }
    }
}
