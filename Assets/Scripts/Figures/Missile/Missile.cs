﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Missile : Figure
{
    protected float duration = 1;
    protected float startTime;
    public string targetTag;
    public Vector2 direction2d;
    public int Dmg;
    new protected virtual void Start()
    {
        startTime = Time.time;
    }

    void Update()
    {
        if (Time.time - startTime > duration)
        {
            Destroy(gameObject);
        }
        transform.Translate(direction2d*speed*Time.deltaTime);
    }
    protected virtual void OnTriggerEnter2D(Collider2D collision)
    {
        
        if (collision.tag == targetTag)
        {
            collision.GetComponent<Opponent>().TakeDamage(Dmg);
            Destroy(gameObject);

        }
    }
}
