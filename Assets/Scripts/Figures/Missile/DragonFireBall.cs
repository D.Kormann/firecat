﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class DragonFireBall : Missile
{
    public Figure targetFigure;
    public Transform target;
    Vector3 move;
    protected override void Start()
    {
        base.Start();
        target = targetFigure.transform;
    }
    private void Update()
    {
        if (target == null)
        {
            Destroy(gameObject);
        }
        move = target.position - transform.position;
        if (move.magnitude <= (speed * Time.deltaTime))
        {
            targetFigure.TakeDamage(Dmg);
            Destroy(gameObject);
        }
        transform.Translate(move.normalized * speed * Time.deltaTime);
        
    }
    protected virtual new  void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Ground")
        {
            Destroy(gameObject);
        }
       
    }
}
