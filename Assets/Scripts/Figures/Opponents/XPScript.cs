﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XPScript : MonoBehaviour
{
    public int XP;
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Player" || collision.collider.tag == "Drake")
        {
            Interface.FoodCount += XP;
            Destroy(gameObject);
        }
    }
}
