﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Block : Opponent
{
    Rigidbody2D rgdbd;
    new private void Start()
    {
        base.Start();
        rgdbd = GetComponent<Rigidbody2D>();
    }
    
    public override void TakeDamage(int Dmg)
    {
        eyeColor = Color.red;
        base.TakeDamage(Dmg);
        nice = false;
    }

}
