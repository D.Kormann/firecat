﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Opponent : Figure
{
    public Color eyeColor;
    public SpriteRenderer[] Eyes;
    public Transform body;
    public int Dmg;
    public bool nice;
    public int directiony;

    // sets level scaling and values
    public void Spawn(int Level)
    {
        this.Level = Level;
        OpponentScalings.ScaleUp(this);
        transform.localScale = transform.localScale * Mathf.Sqrt(Level);
        if (Eyes.Length > 0)
        {
            eyeColor = Eyes[0].color;
        }
    }
    public override void TakeDamage(int Damage)
    {
        base.TakeDamage(Damage);
        foreach (SpriteRenderer eye in Eyes)
        {
            eye.color = new Color(eyeColor.r, eyeColor.g, eyeColor.b, (float)HP / MaxHP);
        }
    }
    public override void Die()
    {

        XPScript xpp = Instantiate(body, transform.position, Quaternion.identity).GetComponent<XPScript>();
        xpp.transform.localScale = transform.localScale;
        xpp.XP = XP;
        base.Die();
        
    }

    virtual protected void Update()
    {
        //Verfolgungsmodus
        if (!nice)
        {
            float dist = player.transform.position.x - transform.position.x;
            SetDirection(dist);
            if (Mathf.Abs(dist) > 0.5f && Time.timeScale != 0)
            {
                transform.Translate(Vector3.right * direction * speed * Time.deltaTime);
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (!nice)
        {
            //DoDmg
            if (collision.tag == "Player")
            {
                print("bla");
                player.TakeDamage(Dmg);
                Destroy(gameObject);
            }
            //Awaken others
            else if (collision.tag == "Opponent")
            {
                collision.GetComponent<Opponent>().TakeDamage(0);
            }
        }
    }
    //setting up event listener
    private void OnEnable()
    {
        Company.OnWakeUp += wakeUp;
    }
    private void OnDisable()
    {
        Company.OnWakeUp -= wakeUp;
    }
    private void wakeUp()
    {
        TakeDamage(0);
    }



}
