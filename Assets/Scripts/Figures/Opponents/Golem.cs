﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Golem : Opponent
{
    Rigidbody2D rb;
    new private void Start()
    {
        base.Start();
        HP = MaxHP;
        rb = GetComponent<Rigidbody2D>();
    }
    public override void TakeDamage(int Damage)
    {
        eyeColor = Color.yellow;
        base.TakeDamage(Damage);
        nice = false;
    }
}
