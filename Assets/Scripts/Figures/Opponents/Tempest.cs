﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tempest : Opponent
{
    Rigidbody2D rb;
    public float StartOrtx;
    public float StartOrty;
    public float bis;

    new private void Start()
    {
        speed = 1.5f;
        Spawn(SkyWorldScript.Stage);
        base.Start();
        HP = MaxHP;
        rb = GetComponent<Rigidbody2D>();
        StartOrtx = transform.position.x;
        StartOrty = transform.position.y;
        bis = StartOrtx + 5;
    }

    //ParadeFkt
    new private void Update()
    {
        base.Update();
        if (nice)
        {
            //Patroullieren
            if (direction == 1) {
                if (transform.position.x >= bis)
                {
                    SetDirection(-1);
                }
                transform.Translate(Vector2.right * speed * Time.deltaTime);
            }
            if (direction == -1)
            {
                if (transform.position.x <= StartOrtx)
                {
                    SetDirection(1);
                }
                transform.Translate(Vector2.left * speed * Time.deltaTime);
            }
        }

        if (!nice)
        {
            //Verfolgung
            speed = player.speed * 0.9f;
            if (player.transform.position.y > transform.position.y)
            {
                transform.Translate(Vector2.up * speed * Time.deltaTime);
            }
            if (player.transform.position.y < transform.position.y)
            {
                transform.Translate(Vector2.down * speed * Time.deltaTime);
            }
        }
    }
    public override void TakeDamage(int Damage)
    {
        eyeColor = Color.blue;
        base.TakeDamage(Damage);
        nice = false;
    }
}
