﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpponentScalings : MonoBehaviour
{
    public static int[][] DmgScaling = new int[][]{
        new int[] { 10, 12, 13, 15, 16, 18}, //Golem
        new int[] { 5, 7, 10, 14, 17, 20}, //Block
        new int[] {15}//Tempest
    };
    public static int[][] MaxHP = new int[][]{
        new int[] { 40, 50, 65, 75, 90, 100}, //Golem
        new int[] { 25, 30, 35, 45, 50, 60}, //Block
        new int[] {20}//Tempest
    };
    public static int[][] XP = new int[][]{
        new int[] { 10, 12, 13, 15, 16, 18}, //Golem
        new int[] { 5, 7, 10, 14, 17, 20}, //Block
        new int[] {15}//Tempest
    };

    public static float[][] MovementSpeed = new float[][]{
        new float[] { 0.012f, 0.013f, 0.014f, 0.015f, 0.016f, 0.017f}, //Golem
        new float[] { 0.015f, 0.016f, 0.017f, 0.018f, 0.019f, 0.02f}, //Block
        new float[] {0.02f}//Tempest
    };

    public static void ScaleUp(Opponent opponent)
    {
        opponent.Dmg = 7;
        opponent.XP = 5;
        opponent.MaxHP = 40;
        opponent.speed = 1f;
        int x = opponent.Level;
        opponent.Dmg *= x;
        opponent.XP *= x;
        opponent.MaxHP *= x;
        //opponent.speed *= x * x;

        //this part is not used in para version
        /*
        int i = 0;
        if(opponent is Golem)
        {
            i = 0;
        }
        if (opponent is Block)
        {
            i = 1;
        }
        opponent.Dmg = DmgScaling[i][opponent.Level- 1];
        opponent.XP = XP[i][opponent.Level - 1];
        opponent.MaxHP = MaxHP[i][opponent.Level - 1];
        opponent.speed = MovementSpeed[i][opponent.Level - 1];
        */
    }
}
