﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class SpellBook: MonoBehaviour
{
    public Transform fire;
    public Transform holder;

    public Action[] setupSpellBook(Martial m,Animator anim) 
    {

        //declare the array
        Action[] sb = new Action[256];
        //gives all 256 spells a not implemented warning
        for (int i = 0; i < sb.Length; i++)
        {
            sb[i] = () => Debug.Log("spell number " + (i + 1) + "not implemented");
        }
        //start implementing spells

        sb[0] = () => {
            anim.SetTrigger("hit");

            
        };
        sb[1] = () => {
            anim.SetTrigger("kick");
        };
        sb[2] = () => {
            Instantiate(fire, holder);
        };


        return sb;
    }
    public void clear
        ()
    {
        foreach (Transform g in holder.GetComponentInChildren<Transform>())
        {
            Destroy(g.gameObject);
        }
    }
}
