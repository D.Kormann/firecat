﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : Figure
{
    //Controll
    public float Mana;
    public float MaxMana;
    public int MaxXP;
    [Header("Specify your keys here")]
    public KeyCode Right;
    public KeyCode Left, Jump, Player1, Player2, Player3,Player4, Pet1, Pet2, Pet3;
    

    //Movement
    protected bool ground;
    protected Rigidbody2D rb;

    //Player
    [HideInInspector]
    public float HPRegen;
    [HideInInspector]
    public float ManaRegen;
    [HideInInspector]
    public int QDmg;
    [HideInInspector]
    public int QSpellManaCost;
    [HideInInspector]
    public int WDmg;
    [HideInInspector]
    public int WSpellManaCost;
    [HideInInspector]
    public int EDmg;
    [HideInInspector]
    public int ESpellManaCost;
    [HideInInspector]
    public int RDmg;
    [HideInInspector]
    public int RSpellManaCost;
    

    //Company
    

    private void Awake()
    {
        player = this;

    }
    new protected void Start()
    {
        base.Start();
        rb = GetComponent<Rigidbody2D>();
        PlayerScalings.ScaleUp(this);
        if (Core.PLevel != 0)
        {
            Level =(int) Core.PLevel;
            HP = Core.HP;
            Mana = Core.Mana;
        }
    }
    public override void Die()
    {
        Interface.FoodCount = 0;
        Core.Fury = 0;
        Core.Stage = 1;
        Core.CLevel = Core.PLevel = 1;
        SaveSystem.safe();
        SceneManager.LoadScene("StartMenu");
        

    }
    public virtual void TakeXP(int x)
    {
        XP += x;
        while (XP >= MaxXP)
        {
            LevelUp();
        }
    }
    protected virtual void LevelUp()
    {
        transform.localScale *= 1.1f;
        Level++;
        XP -= MaxXP;
    }
    protected virtual void Update()
    {
        //Movement
        if (Input.GetKey(Left))
        {
            SetDirection(-1);
            transform.Translate(Vector2.right * speed * direction * Time.deltaTime);


        }
        else if ( Input.GetKey(Right))
        {
            SetDirection(1);
            transform.Translate(Vector2.right * speed * direction * Time.deltaTime);

        }
        if (Input.GetKeyDown(Jump))
        {
            if (ground)
            {
                ground = false;
                rb.velocity = new Vector2(rb.velocity.x, 6);
            }
        }

            //Abilitys

        //PlayerQSpell
        if (Input.GetKeyDown(Player1))
        {
            if (Mana >= QSpellManaCost)
            {
                ManaReduction(QSpellManaCost);
                QSpell();
            }
        }

        //PlayerWSpell
        if (Input.GetKeyDown(Player2))
        {
            if (Mana >= WSpellManaCost)
            {
                ManaReduction(WSpellManaCost);
                WSpell();
            }
        }

        //CompanyASpell
        if (Input.GetKeyDown(Pet1))
        {
            pet.ASpell();
        }

        // ManaRegen && HPRegen
        if (Mana < MaxMana)
        {
            Mana += ManaRegen * Time.deltaTime*60;
        } 
        if (HP < MaxHP)
        {
            HP += HPRegen * Time.deltaTime * 60;
        }
    }

   

    // SpellFkts
    protected virtual void QSpell()
    {

    }
    protected virtual void WSpell()
    {

    }
    public void ManaReduction(int SpellManaCost)
    {
        Mana -= SpellManaCost;
    }

    //MovementFkt
    protected void OnCollisionStay2D(Collision2D collision)
    {
        ground = true;
    }

}
