﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mage : Player
{
    public Transform fireball;
    public Transform waterpunch;


    new public void Start()
    {
        base.Start();
        MaxMana = 100;
        MaxHP = 50;
    }


    override protected void QSpell()
    {
        Missile ball;
        ball= Instantiate(fireball, transform.position+Vector3.right*direction, Quaternion.identity).GetComponent<Missile>();
        ball.targetTag = "Opponent";
        ball.direction2d = new Vector3(direction,0);
        ball.Dmg = QDmg;
}

    protected override void WSpell()
    {
        Missile WaterPunch;
        WaterPunch = Instantiate(waterpunch, transform.position + Vector3.right * direction, Quaternion.identity).GetComponent<Missile>();
        WaterPunch.targetTag = "Opponent";
        WaterPunch.direction2d = new Vector3(direction, 0);
        WaterPunch.direction = direction;
        WaterPunch.Dmg = WDmg;
    }

}
