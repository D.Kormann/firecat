﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScalings : MonoBehaviour
{
    //------------LevelUp Conditions----------------------------------------------------------------------------------------------

    public static int[][] XPNeeded = new int[][] {
        new int[]{ 20, 40, 60, 80, 100, 120}

    };
        
    //------------HP & Mana Scalings---------------------------------------------------------------------------------------------

    //HP
    public static int[][] HPScaling = new int[][] {
        new int[] { 0, 10, 10, 10, 15, 15 } //Mage
    };

    //Mana
    public static int[][] ManaScaling = new int[][] {
        new int[] { 0, 12, 13, 15, 17, 18  } //Mage
    };

    //HPRegen
    public static float[][] HPRegen = new float[][] {
        new float[] {0.02f, 0.025f, 0.03f, 0.035f, 0.04f, 0.045f } //Mage
    }; 

    //ManaRegen
    public static float[][] ManaRegen = new float[][] {
        new float[] {0.03f, 0.035f, 0.045f, 0.055f, 0.06f, 0.065f } //Mage
    };

    //-----------SpellScalings--------------------------------------------------------------------------------------------------

    //Q
    public static int[][] QDmgScaling = new int[][]{
        new int[] { 10, 12, 13, 15, 16, 18} // Mage
    };
    public static int[][] QSpellManaCost = new int[][] {
        new int[] { 5, 5, 7, 9, 11, 13} //Mage
    };

    //W
    public static int[][] WDmgScaling = new int[][] {
        new int[] { 5, 6, 7, 8, 10, 11} // Mage
    };
    public static int[][] WSpellManaCost = new int[][] {
        new int[] {10, 12, 13, 14, 16, 17 } //Mage
    };

    //E
    public static int[] EDmgScaling;
    public static int[] ESpellManaCost;

    //R
    public static int[] RDmgScaling;
    public static int[] RSpellManaCost;




    //------------------------ScaleUpFkt--------------------------------------------------------------------------------
    public static void ScaleUp(Player player)
    {
        int i = 0;
        if(player is Mage)
        {
            i = 0;
        }

        player.MaxXP = XPNeeded[i][player.Level - 1];
        player.MaxHP += HPScaling[i][player.Level - 1];
        player.HP += HPScaling[i][player.Level - 1];
        player.MaxMana += ManaScaling[i][player.Level - 1];
        player.Mana += HPScaling[i][player.Level - 1];
        player.ManaRegen = ManaRegen[i][player.Level - 1];
        player.HPRegen = HPRegen[i][player.Level - 1];
        player.QSpellManaCost = QSpellManaCost[i][player.Level - 1];
        player.QDmg = QDmgScaling[i][player.Level - 1];
        player.WSpellManaCost = WSpellManaCost[i][player.Level - 1];
        player.WDmg = WDmgScaling[i][player.Level - 1];
    }




}
