﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Martial : Player
{
    Animator anim;
    //this is a counter to determine which spell is used next;
    //spell is determined by four keys pressed up to 4 times in a row;
    //allowing for 256 different combinations
    byte Spell = 0b_0000_0000;

    //stores the time the last keywaspressed to wait for next input
    float lastKeyTime;
    float maxKeyTime=.2f;
    public int keyCount;
    CharacterController2D controller;
    float horizontalMove;
    bool jump;
    Action[] spellBook=new Action[256];//this array stores all the spells you can cast
    // Start is called before the first frame update
    new void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        controller = GetComponent<CharacterController2D>();
        anim = GetComponentInChildren<Animator>();
        spellBook=GetComponent<SpellBook>().setupSpellBook(this, anim);
    }

    

    // Update from playeris completely overwritten, because not needed
    new void Update()
    {
        
        horizontalMove = Input.GetAxisRaw("Horizontal");
        anim.SetBool("run",horizontalMove!=0);
        //SetDirection(horizontalMove);
        jump = Input.GetKeyDown(Jump);
        
        if(keyCount>0)
        {
            if (Time.time - lastKeyTime > maxKeyTime)
            {
                releaseSpell(Spell);
                keyCount = 0;
            }
        }
        
        if (Input.GetKeyUp(Player1))
        {
            handleInput(0);
        }
        if (Input.GetKeyUp(Player2))
        {
            handleInput(1);
        }if (Input.GetKeyUp(Player3))
        {
            handleInput(2);
        }if (Input.GetKeyUp(Player4))
        {
            handleInput(3);
        }
        
    }
    private void handleInput(byte key)
    {
        //counting keys pressed after4 keys the spellmust be released
        keyCount++;
        //shifting byte tog create space for next Key
        Spell <<= 2;
        //next key is stored;
        Spell += key;
        //check if 4 keys are pressed 
        if (keyCount%4 == 0)
        {
            releaseSpell(Spell);
        }
        //update the lastkeytime
        lastKeyTime = Time.time;
    }

    private void releaseSpell(byte spell)
    {
        spellBook[Spell]();
        print(Spell);
        Spell = 0;
    }
    private void FixedUpdate()
    {
        controller.Move(horizontalMove*Time.fixedDeltaTime*speed, false, jump);
        if (jump) jump = false;
        

    }
    
}
