﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Figure : MonoBehaviour
{
    public static Player player;
    public static Company pet;
    [Header("Vital values")]
    public int Level;
    public int XP;
    public int MaxHP;
    public float HP;
    public float speed;
    [Range(-1,1)]
    public int direction = 1;
    protected virtual void Start()
    {
        Level = 1;
        HP = MaxHP;
        
    }
    
    
    public virtual void TakeDamage(int Damage)
    {
        HP -= Damage;
        if (HP <= 0)
        {
            Die();
        }
        
    }
    public virtual void Die()
    {
        Destroy(gameObject);
    }
    protected void SetDirection(int movement)
    {
        SetDirection((float)movement);
    }
    protected void SetDirection(float movement)
    {
        if (movement < 0)
        {
            if (direction == 1)
            {
                direction = -1;
                transform.localScale = new Vector3(direction * Mathf.Abs(transform.localScale.x), transform.localScale.y);
            }
        }
        else if (movement > 0)
        {
            if (direction == -1)
            {
                direction = 1;
                transform.localScale = new Vector3(direction * Mathf.Abs(transform.localScale.x), transform.localScale.y);
            }
        }

    }

}
