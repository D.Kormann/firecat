﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CompanyScalings : MonoBehaviour
{
    //Company Scalings 

        //LevelUpConditions
    public static int[][] Dmg = new int[][] {
        new int[] { 10, 20, 35, 55, 80, 120 } //Cat
    };
    public static int[][] MaxXP = new int[][] {
        new int[] { 20, 40, 60, 80, 100, 120  } //Cat
    };

        //Fury

    public static int[][] MaxFury = new int[][] {
        new int[] { 1000, 1350, 1750, 2150, 2600, 3000 } //Cat
    };
    public static float[][] FuryGeneration = new float[][] {
        new float[] { 0.5f, 0.55f, 0.6f, 0.65f, 0.7f, 0.8f} //Cat
    };
    public static float[][] FuryCost = new float[][] {
        new float[] { 10, 12, 15, 18, 22, 26 } //Cat
    };



    public static void ScaleUp(Company company)
    {
        if (company.paraScene)
            ScaleUpPara(company);
        else {
            int i = 0;
            if (company is Cat)
            {
                i = 0;
            }
            company.Dmg = Dmg[i][company.Level - 1];
            company.MaxFury = MaxFury[i][company.Level - 1];
            company.FuryCost = FuryCost[i][company.Level - 1];
            company.FuryGeneration = FuryGeneration[i][company.Level - 1];
        }
    }


    //para scene
    //int para scene all stats are calculated based on math functions
    static float StartDmg, StartMaxFury, StartFuryCost, StartFuryGeneration, StartMaxXP;
    public static void ScaleUpPara(Company company)
    {
        //in first time calling te function all values are set to given values so they can be changed in scene view
        if (StartDmg == 0)
        {
            StartDmg = 5;
            StartMaxFury =1000;
            StartFuryCost = 10;
            StartFuryGeneration = 0.5f;
            StartMaxXP = 20;
        }
        //stats are beeing calculated based on level
        if (company is Cat ||company is Drake)
        {
            int x = company.Level;
            company.Dmg = (int)StartDmg * x;
            company.MaxFury = StartMaxFury * x*x;
            company.FuryCost = StartFuryCost*x;
            company.FuryGeneration = StartFuryGeneration*x;
  

        }
    }
}
