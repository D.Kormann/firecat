﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cat : Company
{
    public bool hunting;

    override protected void Follow()
    {
        if (!nice)
        {
            
            if (hunting)
            //running afterplayerlike mad
            {


                Vector2 dist = player.transform.position - transform.position;
                if (dist.sqrMagnitude < .2)
                {
                    player.GetComponent<Figure>().TakeDamage(Dmg);
                    hunting = false;
                    Fury -= 2 * FuryCost;
                    
                }

                transform.Translate(dist.normalized * speed*Time.deltaTime);
                SetDirection(dist.x);
            }
            else
            //running away mad
            {
                Vector2 dist = player.transform.position - transform.position;
                if (dist.sqrMagnitude > 4)
                {
                    hunting = true;
                }
                transform.Translate(-dist.normalized * speed*Time.deltaTime);
                SetDirection(-dist.x);

            }
        }
        else
        {
            //run at the Opponent
            if (hunting)
            {
                Vector2 dist = Physics2D.ClosestPoint(transform.position, targetColl) - new Vector2(transform.position.x, transform.position.y);
                if (dist.sqrMagnitude < 1)
                {
                    target.GetComponent<Figure>().TakeDamage(Dmg);
                    hunting = false;
                }
                else if ((transform.position - rider.position).magnitude > range)
                {
                    hunting = false;
                }
                transform.Translate(dist.normalized * speed*Time.deltaTime);
                SetDirection(dist.x);
            }
            else
            //follow the player
            {

                Vector2 dist = rider.position - transform.position;
                if (dist.sqrMagnitude > 200)
                {
                    //teleport back to the player
                    transform.position = player.transform.position;
                }
                if (dist.magnitude > 1.5f)
                {
                    //follow smoothly
                    transform.Translate(dist * 0.2f * speed*Time.deltaTime);
                    SetDirection(dist.x);
                }
            }
        }
    }
    public override void ASpell()
    {
        base.ASpell();
        
        if (FindOpponent())
        {
            if (nice)
            {
                targetColl = target.GetComponent<Collider2D>();
                hunting = true;
                Fury += FuryCost;
            }
        }
    }
    
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(rider.position, range);
    }
}