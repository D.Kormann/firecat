﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Company : Figure
{
    //ParaScene
    public bool paraScene;
    //CompanyScalings
    public int Dmg;
    public float Fury, FuryGeneration, MaxFury, FuryPerFood, FuryCost;
    //Movement
    public Transform rider;
    public bool nice;
    public bool following;
    public float range;
    Animator anim;
    protected Transform target;
    protected Collider2D targetColl;
    //rageTransformation
    public Material mat;
    public GameObject redEyes;
    public LayerMask human, opponent;
    private LayerMask whatIsEnemy;
   


    //setting up event system for waking opponents
    public delegate void wakeup();
    public static event wakeup OnWakeUp;
    private void Awake()
    {
        pet = this;
    }
    new virtual protected void Start()
    {

        Level = 1;
        FuryPerFood = 30;
        anim = GetComponent<Animator>();
        GetGood();
        whatIsEnemy = opponent;
        nice = true;
        base.Start();
        direction = 1;
        following = true;
        
        //getting all values set up in case of scene change
        if (Core.PLevel != 0)
        {
            Level =(int) Core.CLevel;
            Fury = Core.Fury;
            if (Core.nice != true)
            {
                nice = false;
                targetColl = player.GetComponent<Collider2D>();
                target = transform;
                mat.color = Color.black;
                
            }
            ScaleUp();
        }
    }

    //fury management
    protected void Update()
    {
        if (nice)
        {
            Fury += FuryGeneration*Time.deltaTime*60;
            if (Fury > MaxFury)
            {
                anim.SetTrigger("Bamm");
                Fury = MaxFury - FuryGeneration;
                nice = false;
            }
        }
        else
        {
            Fury -= FuryGeneration * 120*Time.deltaTime;
            if (Fury <= 0)
            {
                GetGood();

            }
        }
        
        
    }
    //calls Follow if following true
    protected void LateUpdate()
    {
        if (following)
        {
            Follow();
        }
    }
    protected virtual void Follow()
    {
        
    }
    public virtual void ASpell()
    {

    }

    //finds the next opponent in radius and stores its Transform in target and its Collider targetColl
    //returns false if none in range
    public virtual bool FindOpponent()
    {
        bool found = false;
        Collider2D[] OpponentsInRange;
        float sqrDist;
        Transform nextOpponent = null;

        OpponentsInRange = Physics2D.OverlapCircleAll(rider.position, range,whatIsEnemy);
        if (OpponentsInRange.Length > 0)
        {
            found = true;
            nextOpponent = OpponentsInRange[0].transform;
            sqrDist = Vector3.SqrMagnitude(nextOpponent.transform.position - rider.position);
            float newDist;

            foreach (Collider2D collider in OpponentsInRange)
            {
                newDist = Vector3.SqrMagnitude(collider.transform.position - rider.position);
                if (newDist < sqrDist)
                {
                    sqrDist = newDist;
                    nextOpponent = collider.transform;
                    targetColl = collider;
                }
            }
        }

        if (found)
        {
            target = nextOpponent;
            
        }
        return found;
    }

    //sets up the rage visuals
    private void GetBad()
    {
        nice = false;
        targetColl = player.GetComponent<Collider2D>();
        //target = player.transform;
        target = transform;
        mat.color = Color.black;
        redEyes.SetActive(true);
        OnWakeUp();
    }
    protected void GetGood()
    {
        nice = true;
        Fury = 0;
        mat.color = Color.white;
        //redEyes.SetActive(false);
        
    }
    public virtual void ScaleUp()
    {
        //transform.localScale = Vector3.one * Mathf.Pow(1.1f, Level);
        CompanyScalings.ScaleUp(this);
    }
}
