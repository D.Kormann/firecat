﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Drake : Company
{
    public Transform FireBall;
    Vector3 targetSpot;
    public Vector3 vec;
    public  Transform P;
    protected override void Start()
    {
        
        base.Start();
        CompanyScalings.ScaleUp(this);
        P = rider;
        rider = player.transform;
    }
    protected override void Follow()
    {
        targetSpot = P.position + Vector3.up * 1 + Vector3.left*player.direction -transform.position;
        if (targetSpot.magnitude < .1f)
        {
            
        }
        else
        {
            targetSpot =targetSpot*0.2f + targetSpot.normalized;
        }
        transform.Translate(targetSpot * speed*Time.deltaTime);
        SetDirection(player.transform.position.x-transform.position.x);

    }
    public override void ASpell()
    {
        
        base.ASpell();
        
        if (FindOpponent(opponent))
        {

            DragonFireBall f = Instantiate(FireBall, transform.position, Quaternion.identity).GetComponent<DragonFireBall>();
            f.targetFigure = target.GetComponent<Figure>();
        
        }
        
    }
    Collider2D[] OpponentsInRange;
    float sqrDist;
    //finds the next enemy transfrom stores it in target. returns null if no enemy in range
    protected bool FindOpponent(LayerMask whatIsEnemy)
    {
        
        OpponentsInRange = Physics2D.OverlapCircleAll(player.transform.position, range, whatIsEnemy);
        if (OpponentsInRange.Length > 0)
        {
            target = OpponentsInRange[0].transform;
            sqrDist = Vector3.SqrMagnitude(target.transform.position - rider.position);
            float newDist;

            foreach (Collider2D collider in OpponentsInRange)
            {
                newDist = Vector3.SqrMagnitude(collider.transform.position - rider.position);
                if (newDist < sqrDist)
                {
                    sqrDist = newDist;
                    target = collider.transform;
                }
            }
        }

        
        return OpponentsInRange.Length!=0;
    }
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, range);
    }
}
