﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CoreData
{
    public int catLevel, petLevel,stage,food;
    public float mana, hp, fury;
    public bool nice;
    public CoreData()
    {

        this.catLevel = (int)Core.CLevel;
        petLevel = (int)Core.PLevel;
        mana = Core.Mana;
        hp = Core.HP;
        fury = Core.Fury;
        nice = Core.nice;
        stage = Core.Stage;
        food = Core.Food;
    }
}
