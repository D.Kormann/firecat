﻿using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem
{
   public static void safe()
    {

        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "player.hqxt";
        FileStream stream = new FileStream(path, FileMode.Create);
        CoreData data = new CoreData();
        Debug.Log("saving :" + data.fury);
        formatter.Serialize(stream, data);
        stream.Close();


    }
    public static CoreData loadData()
    {
        string path = Application.persistentDataPath + "player.hqxt";

        if (File.Exists(path))
        {
            BinaryFormatter formatter= new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            CoreData data = formatter.Deserialize(stream)as CoreData;
            Debug.Log("loading :" + data.fury);

            stream.Close();
            return data;
        }
        else
        {
            Debug.LogError("file no found in " + path);
            return null;
        }
    }
}
