﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreA1: Core
{
    
    public int opponentCount, opponentMaxLvl, opponentMinLvl;
    override protected void Start()
    {
        opponentMaxLvl = Stage + 1;
        opponentMinLvl = Stage+1;
        base.Start();

        //spawn random Opponents at random level all over the map
        for (int i = 0; i < opponentCount; i++)
        {
            int k = Random.Range(0, opponents.Length);
            Opponent op = Instantiate(opponents[k], Vector3.Lerp(sceneStart.position, sceneEnd.position, Random.Range(0f, 1f)), Quaternion.identity);
            op.Spawn(Random.Range(opponentMinLvl, opponentMaxLvl));
            
        }
        
    }
}
