﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Core : MonoBehaviour
{

    //store data for scene change
    public Company cat;
    public static float HP, Mana, Fury,PLevel,CLevel;
    public static bool nice;
    public Opponent[] opponents;
    public static Player player;
    public static Company pet;
    public static int Stage = 1,Food;
    public Transform sceneStart,sceneEnd;
    public Transform sky;
    public float PETFURY;
    private Vector3 skyOffset;

    private void Awake()
    {
        
        Stage = 1;
        CoreData data = SaveSystem.loadData();
        if (data != null)
        {
            HP = data.hp;
            Mana = data.mana;
            Fury = data.fury;
            PLevel = data.petLevel;
            CLevel = data.catLevel;
            nice = data.nice;
            Stage = data.stage;
            Interface.FoodCount = data.food;
            print("fury  : " + Fury);

            
        }
        
    }
    protected virtual void Start()
    {
        
        Time.timeScale = 1;
        player = Figure.player;
        pet = Figure.pet;
        skyOffset = sky.position - player.transform.position;
    }
    protected virtual void Update()
    {
        sky.position = player.transform.position/2 + skyOffset;

    }
    public static void nextScene()
    {

        print("");
        print("");
        print("petfury: " + Figure.pet.Fury);
        Time.timeScale = 0;
        Food = Interface.FoodCount;
        HP = Figure.player.HP;
        Mana = Figure.player.Mana;
        Fury = Figure.pet.Fury;
        PLevel = Figure.player.Level;
        CLevel = Figure.pet.Level;
        nice = Figure.pet.nice;
        print("core saving fury: " + Fury);
        SaveSystem.safe();
        SceneManager.LoadScene("SkyWorld");
    }
}
